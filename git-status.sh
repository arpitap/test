cd /home/software/Desktop/test;
if [ "$(git status  --porcelain)" ]; then
 echo " Changes";
 git checkout -b client_update;
 git add .;
 git commit -m "client uploaded changes";
 git checkout develop;
 git merge client_update;
 git push origin develop;
 git checkout master;
 git merge client_update;
 git branch -d client_update;
else
  echo " No changes";
fi
